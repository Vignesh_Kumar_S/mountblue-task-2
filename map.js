function map(items)
{
   if( !items[0] )
      return [];
      
   const local = [];
   for( let i=0; i<items.length; i++ )
      local.push(items[i]);
   return local;
}

module.exports = map;