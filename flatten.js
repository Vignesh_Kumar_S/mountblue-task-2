const reduce = require("./reduce");

function flatten(nestedArray,result) 
{
   for( i=0; i<nestedArray.length; i++ )
   {
      if( Array.isArray(nestedArray[i]) )
      {
         flatten(nestedArray[i],result);
      }
      else
      {
         result.push(nestedArray[i]);
      }
   }
   return result;
 }
 

module.exports = flatten;