function each(elements, cb) 
{
   if( !elements[0] )
      return [];

   for( let i=0; i<elements.length; i++ )
      cb.push(elements[i]);
   return cb;
}

module.exports = each;