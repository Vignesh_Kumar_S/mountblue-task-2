function reduce(elements, cb, startingValue) 
{
   let a;let i=0;
   if( startingValue === undefined )
   {
      a = elements[0];
      i=1;
   }
   else
   {
      a = startingValue;
   }

   for ( i; i < elements.length; i++ ) 
   {
      const currentItem = elements[i];
      a = cb(a, currentItem);
   }
   return a;
}

module.exports = reduce;
