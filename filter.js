function filter( elements,cb )
{
   const local = [];
   for( i=0; i<elements.length; i++ )
   {
      if(cb(elements[i]))
         local.push(elements[i]);
   }
   if( local[0] === undefined )
      return undefined;
   else
      return local;
}

module.exports = filter;